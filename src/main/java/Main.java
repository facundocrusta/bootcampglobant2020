import activities.inheritance.Animal;
import activities.inheritance.Bird;
import activities.inheritance.Dog;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        final ArrayList<Animal> animals = new ArrayList<>();

        animals.add(new Dog(4, "Coli", "Black"));
        animals.add(new Bird(2, "Canary",  "Vaned Feathers"));

        for (Animal animal : animals) {
            System.out.println(animal.move());
            System.out.println(animal.toString());
        }
    }
}
