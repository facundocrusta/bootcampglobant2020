package activities.inheritance;

public abstract class Animal {

    private int numberOfLegs;
    private String race;

    public Animal(int numberOfLegs, String race) {
        this.numberOfLegs = numberOfLegs;
        this.race = race;
    }

    public abstract String move();

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public String getRace() {
        return race;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "numberOfLegs='" + numberOfLegs + '\'' +
                ", race='" + race + '\'' +
                '}';
    }
}
