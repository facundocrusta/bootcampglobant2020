package activities.inheritance;

public class Dog extends Animal {


    private String furColor;

    public Dog(int numberOfLegs, String race, String furColor) {
        super(numberOfLegs, race);

        this.furColor = furColor;
    }

    @Override
    public String move() {
        return "Dog is moving...";
    }

    public String getFurColor() {
        return furColor;
    }
}
