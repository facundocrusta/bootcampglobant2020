package activities.inheritance;

public class Bird extends Animal {

    private String featherType;


    public Bird(int numberOfLegs, String race, String featherType) {
        super(numberOfLegs, race);

        this.featherType = featherType;
    }

    @Override
    public String move() {
        return "Bird is moving...";
    }
}
