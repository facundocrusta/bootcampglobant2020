package activities.inheritance;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DogTest {

    private Dog dog;

    @Before
    public void setUp() {
        dog = new Dog(4, "Coli", "Black");
    }

    @Test
    public void shouldSuccessfullyGetNumberOfLegs() {
        assertEquals(4, dog.getNumberOfLegs());
    }

    @Test
    public void shouldSuccessfullyGetRace() {
        assertEquals("Coli", dog.getRace());
    }

    @Test
    public void shouldSuccessfullyGetFurColor() {
        assertEquals("Black", dog.getFurColor());
    }
}
